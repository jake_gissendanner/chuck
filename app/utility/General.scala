package utility

import play.api.Play

import scala.slick.jdbc.JdbcBackend._

object General {

  var gl = Play.current.configuration.getString("glog.gophermap")

  def SlickSession[T](f: Session => T): T = {
    import play.api.Play.current
    play.api.db.slick.DB.withSession(f)
  }

  def Transaction[T](f: Session => T) = {
    import play.api.Play.current
    play.api.db.slick.DB.withTransaction(f)
  }

}