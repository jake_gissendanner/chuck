package utility

import org.joda.time.{DateTimeZone, DateTime}

object DateTimeTypes {

  case class LocalDateTime(date: DateTime)

  case class UTCDateTime(date: DateTime) {
    def toLocal(tz: DateTimeZone): LocalDateTime = {
      LocalDateTime(date.toDateTime(tz))
    }
  }

  object UTCDateTime {
    def now: UTCDateTime = UTCDateTime(DateTime.now(DateTimeZone.UTC))
    def fromTimestamp(ts: Long) = UTCDateTime(new DateTime(ts,DateTimeZone.UTC))
  }
}