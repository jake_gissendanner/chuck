package models

import org.joda.time.format.DateTimeFormat
import play.api.db.slick.Config.driver.simple._
import play.api.mvc.Controller
import scala.slick.lifted.Tag
import org.joda.time.{DateTime}

case class PostSql (id: Int, title: String, content: String, date: String) {
  def asTextPost: controllers.Post = controllers.TextPost(title, content, "post", date)
}

class PostsTable(tag: Tag) extends Table[PostSql](tag, "posts_table") {
  def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
  def title = column[String]("title")
  def content = column[String]("content")
  def date = column[String]("date")
  def * = (id, title, content, date) <> (PostSql.tupled, PostSql.unapply)
}

object PostSqlObj {

  val dtFormat = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss") //MySQL DateTime Format.
  val query = TableQuery[PostsTable]

  def create(title: String, content: String)(implicit s: Session):Int =
    query += PostSql(-1, title, content, dtFormat.print(new DateTime()))

  def getAllPosts(reverse: Boolean)(implicit s: Session):List[PostSql] = {
    reverse match {
      case true   => query.sortBy(_.date.asc).list
      case _      => query.sortBy(_.date.desc).list
    }
  }

}
