package models

import play.api.db.slick.Config.driver.simple._
import scala.slick.lifted.Tag

case class Setting (id: Int, sType: String, value: String)

class SettingsTable(tag: Tag) extends Table[Setting](tag, "settings") {
  def id = column[Int]("id", O.PrimaryKey) // This is the primary key column
  def sType = column[String]("sType")
  def value = column[String]("value")
  def * = (id, sType, value) <> (Setting.tupled, Setting.unapply)
}

object Settings {

  val query = TableQuery[SettingsTable]

  def getSetting(sType: String)(implicit s: Session): Option[String] = {
    query.filter(_.sType === sType).firstOption.map(_.value)
  }

  def getAllSettings()(implicit s: Session): List[Setting] = {
    query.list
  }

  def updateSetting(id: Int, value: String)(implicit s: Session): Unit = {
    val sq = for { s <- query if s.id === id } yield s.value
    sq.update(value)
  }

}

