package controllers

import java.text.SimpleDateFormat
import java.util.Calendar
import play.api._
import play.api.Play.current
import java.sql._
import play.api.mvc._
import play.api.libs.json._
import java.io._
import models.{PostSqlObj, Settings}
import play.api.db.slick.DBAction
import utility.General._
import scala.util.Try

object Generate extends Controller {

  var gl = Play.current.configuration.getString("glog.gophermap").getOrElse("/var/gopher/gophermap")
  val gw = Play.current.configuration.getInt("glog.textWidth").getOrElse(70)
  val crlf = sys.props("line.separator")

  def genGophermap = Action { implicit request =>
    val writer = Try { new PrintWriter(new File(s"$gl")) }
    writer.map{ w =>
      w.write(getHeader + crlf + getContent + crlf + getFooter)
      w.close()
      Ok(Json.obj("success" -> true, "message" -> s"Gophermap Generated at '$gl'."))
    }.recover { case error =>
      Ok(Json.obj("success" -> false, "message" -> s"Gophermap not generated. Error: ${error.getMessage}"))
    }.getOrElse {
      Ok(Json.obj("success" -> false, "message" -> s"Gophermap not generated. Error: No error given."))
    }
  }

  // -- Methods --
  def getHeader:String = SlickSession { implicit session =>
    formatText(
      Settings.getSetting("main_title").getOrElse("No Title Set"), "="
    ) + formatText(
      Settings.getSetting("main_intro").getOrElse("No Intro Set"), ""
    )
  }

  def getContent:String = SlickSession { implicit session =>
    var blogTitle = s"[ Blog Posts ]"
    blogTitle = blogTitle.concat(getStringLine("_", gw - blogTitle.size))

    val allPosts = PostSqlObj.getAllPosts(true).foldLeft(""){ case (acc, post) =>
      acc + s"$crlf${formatPostToString(post.asTextPost)}"
    }

    s"$blogTitle$crlf$allPosts$crlf"
  }

  def getFooter:String =
    s"${crlf}Generated ${new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime())}"


  // -- Helper Methods --

  def getDataToMap(rs: ResultSet): Option[Post] = {
    rs.getString("PostType") match {
      case "t" => Option(TextPost(rs.getString("title"), rs.getString("content"), rs.getString("postType"), rs.getString("timestamp")))
      case _ => None
    }
  }

  def formatPostToString(post: Post):String = {
    post match {
      case TextPost(title, content, postType, timestamp) => s"${getBlogTitleFormat(title, timestamp)}$crlf$content$crlf"
      case _ => ""
    }
  }

  def formatText(text: String, wrapper: String):String = {
    val wrap = getStringLine(wrapper, (if (text.size > gw) gw else text.size))
    s"$wrap$crlf${wordWrap(text)}$crlf$wrap$crlf"
  }

  def getStringLine(lineString: String, lineLength: Int):String = {
    var wrap = ""
    for (i <- 0 until lineLength) wrap = wrap.concat(lineString)
    wrap
  }

  def getBlogTitleFormat(title: String, timestamp: String):String = {
    val left = "[ "
    val right = " ]"
    val spaceSize = gw - (title.size + timestamp.size + left.size + right.size)
    println(spaceSize)
    s"/${getStringLine("-", gw - 2)}\\${crlf}| $title${getStringLine(" ", spaceSize)}$timestamp |${crlf}\\${getStringLine("-", gw - 2)}/"
  }

  // https://gist.github.com/ignasi35/5605115
  def wordWrap(input : String):String = {
    input.split (" ").foldLeft (("", 0))(
      (acc, in) =>
        if (in equals "") {
          acc
        } else if ((acc._2 + in.length()) < gw) {
          (acc._1 + " " + in, acc._2 + in.length())
        } else {
          (acc._1 + s"${crlf}" + in, in.length())
        })._1.trim
  }
}