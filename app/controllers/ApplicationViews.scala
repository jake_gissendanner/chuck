package controllers

import models.{Settings, PostSqlObj}
import API._
import play.api.mvc._
import play.api.db.slick.DBAction
import utility.General._
import scalatags.Text.all._
import scalatags.Text.tags2.{title => titletag}

object ApplicationViews extends Controller {

  // -- Main -----------------------------------------------------------------------------------------------------------

  def welcome = SlickSession { implicit request =>
    val blogTitle = Settings.getSetting("main_title").getOrElse("No Title Set")
    val blogIntro = Settings.getSetting("main_intro").getOrElse("No Intro Set")
    div(
      h3(blogTitle),
      h4(blogIntro)
    )
  }

  // -- Add Post. ------------------------------------------------------------------------------------------------------

  def postForms = {
    div(cls:="row")(
      div(cls:="col-lg-11 col-centered")(
        form(name:="saveTextPost", id:="title_field",method:="POST", action:=routes.Application.saveTextPost.url)(
          div(cls:="input-group")(
            label(
              div(cls:="fieldTitle")(
                span(cls:="label label-info")("Title")
              ),
              input(`type`:="text", size:="45", value:="", name:="title", id:="title")
            )
          ),
          div(cls:="input-group")(
            label(
              div(cls:="fieldTitle")(
                span(cls:="label label-info")("Content")
              ),
              textarea(name:="content", id:="content", cls:="content-box", rows:="18", cols:="45"),
              button(cls:="btn btn-default btn-xs", id:="add-html-link")("Add HTML Link to Post")
            )
          ),
          div(cls:="input-group")(
            input(`type`:="submit", value:="Save Post")
          )
        )
      )
    )
  }

  // -- Settings. ------------------------------------------------------------------------------------------------------

  def showSettings = SlickSession { implicit request =>
    div(cls:="row")(
      div(cls:="col-lg-11 col-centered")(
        form(name:="saveSettings", id:="save-settings-form",method:="POST", action:=routes.API.saveSettings.url)(
          div(
            Settings.getAllSettings().map { thisSetting =>
              div(cls:="input-group")(
                label(
                  div(cls:="fieldTitle")(
                    span(cls:="label label-info")(thisSetting.sType)
                  ),
                  input(`type`:="text", size:="45", value:=s"${thisSetting.value}", name:=s"${thisSetting.id}")
                )
              )
            }
          ),
          div(cls:="input-group")(
            input(`type`:="button", cls:="save-settings-button", value:="Save Settings")
          )
        )
      )
    )
  }

  // -- View Posts -----------------------------------------------------------------------------------------------------

  def listPosts = SlickSession { implicit request =>
    ul(
      PostSqlObj.getAllPosts(false).map { thisPost =>
        li(
          h3(thisPost.title),
          p(thisPost.content)
        )
      }
    )

  }

  // -- General. -------------------------------------------------------------------------------------------------------

  def mainHeader = {
    head(
      titletag("Chuck, the blogging Gopher."),
      link(rel:="stylesheet", `type`:="text/css", href:=routes.Assets.at("stylesheets/bootstrap.min.css").url),
      link(rel:="stylesheet", `type`:="text/css", href:=routes.Assets.at("stylesheets/main.css").url),
      link(rel:="stylesheet", `type`:="text/css", href:=routes.Assets.at("stylesheets/toastr.css").url),
      script(src:=routes.Assets.at("javascripts/jQuery-1.11.1.min.js").url),
      script(src:=routes.Assets.at("javascripts/bootstrap.min.js").url),
      script(src:=routes.Assets.at("javascripts/toastr.js").url),
      script(src:=routes.Assets.at("javascripts/addPost.js").url),
      script(src:=routes.Assets.at("javascripts/general.js").url)
    )
  }

  def navbar = {
    div(role:="navigation", cls:="navbar navbar-default")(
      div(cls:="container-fluid")(
        div(cls:="navbar-header")(
          button("data-target".attr:=".navbar-collapse", "data-toggle".attr:="collapse", cls:="navbar-toggle")(
            span(cls:="sr-only")("Toggle navigation"),
            span(cls:="icon-bar"),
            span(cls:="icon-bar"),
            span(cls:="icon-bar")
          ),
          a(href:="#", cls:="navbar-brand")("Chuck (the Gopher Blogger)")
        ),
        div(cls:="navbar-collapse collapse")(
          ul(cls:="nav navbar-nav")(
            li(cls:="active")(a(href:=routes.Application.index.url)("Main")),
            li(cls:="active")(a(href:=routes.Application.viewPosts.url)("View Posts")),
            li(cls:="active")(a(href:=routes.Application.addPost.url)("Add Post")),
            li(cls:="active")(a(href:=routes.Application.settings.url)("Settings")),
            li(button(id:="generate-button", `type`:="button", cls:="btn btn-default", style:="margin: 7px;")("Generate Gophermap"))
          ),
          ul(cls:="nav navbar-nav navbar-right")(
            //li(cls:="dropdown")(
            //  a("data-toggle".attr:="dropdown", cls:="dropdown-toggle", href:="#")("Dropdown", span(cls:="caret"))
            //),
            //li(a(href:="#")("Account")),
            //li(a(href:="#")("Log Out"))
          )
        )
      )
    )
  }

}