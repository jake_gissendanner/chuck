package controllers

import models.{PostSqlObj, Settings}
import play.api.db.slick.DBAction
import play.api.mvc.Controller

object API extends Controller {

  def getBlogSettings = DBAction { implicit request =>
    Ok(upickle.write((
      Settings.getSetting("main_title")(request.dbSession),
      Settings.getSetting("main_intro")(request.dbSession)
      )))
  }

  def getBlogPosts = DBAction { implicit request =>
    Ok(upickle.write(
      PostSqlObj.getAllPosts(true)(request.dbSession)
      //Datetime.now
    ))
  }

  def saveBlogPost = DBAction { implicit request =>
    val title:String = request.queryString.get("title").flatMap(_.headOption).getOrElse("")
    val content:String = request.queryString.get("content").flatMap(_.headOption).getOrElse("")
    Ok(upickle.write(
      PostSqlObj.create(title, content)(request.dbSession)
    ))
  }

  def saveSettings = DBAction { implicit request =>
    val main_title:String = request.queryString.get("main_title").flatMap(_.headOption).getOrElse("")
    val main_intro:String = request.queryString.get("main_title").flatMap(_.headOption).getOrElse("")
    Ok(upickle.map{

      Settings.updateSetting(, main_intro)(request.dbSession)
    }
  }

}
