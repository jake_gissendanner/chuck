package controllers

import play.api._
import play.api.db.slick.DBAction
import play.api.mvc._
import play.api.data._
import play.api.data.Forms._
import play.api.Play.current
import play.twirl.api.Html
import scala.slick.ast.Typed
import scalatags.Text.all._
import scalatags.Text.tags2.{title => titletag}
import upickle._
import models._
import utility.General._
import ApplicationViews._


object Application extends Controller {

  // -- Get Routes. -----------------------------------------------------------------------------------------------------

  def index = Action { implicit request =>
    Ok(
      Html(
        html(
          mainHeader,
          navbar,
          body(
            div(cls:="container")(welcome)
          )
        ).render
      )
    )
  }

  def viewPosts = Action { implicit request =>
    Ok(
      Html(
        html(
          mainHeader,
          navbar,
          body(
            div(cls:="container")(listPosts)
          )
        ).render
      )
    )
  }

  def addPost = Action { implicit request =>
    Ok(
      Html(
        html(
          mainHeader,
          navbar,
          body(
            div(cls:="container")(postForms)
          )
        ).render
      )
    )
  }

  def settings = Action { implicit request =>
    Ok(
      Html(
        html(
          mainHeader,
          navbar,
          body(
            div(cls:="container")(showSettings)
          )
        ).render
      )
    )
  }

  /*def viewBlog = Action { implicit request =>
    Ok(
      Html(
        html(
          mainHeader,
          navbar,
          body(
            div(cls:="container")(
              div(cls:="blog-title"),
              //div(cls:="blog-intro")(getBlogIntro),
              div(cls:="blog-posts")
            )
          )
        ).render
      )
    )
  }*/

  // -- POST Routes. ---------------------------------------------------------------------------------------------------

  def saveTextPost = DBAction { implicit request =>
    Form(tuple( "title" -> text, "content" -> text )).bindFromRequest().fold(
      hasErrors => println("Error saving post."),
      { case (title, content) => PostSqlObj.create(title, content)(request.dbSession) }
    )
    Redirect(routes.Application.viewPosts())
  }

  /*def saveSettings = DBAction { implicit request =>
    Form(tuple( "id" -> number, "value" -> text )).bindFromRequest().fold(
        hasErrors => println("Error saving setting."),
        { case (id, value) => Settings.updateSetting(id, value)(request.dbSession) }
      )
    Redirect(routes.Application.settings())
  }*/



}