package controllers

trait Post
case class TextPost(
  title: String,
  content: String,
  postType: String,
  timestamp: String
) extends Post