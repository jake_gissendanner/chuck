# --- Created by Slick DDL
# To stop Slick DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table `posts_table` (`id` INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,`title` VARCHAR(254) NOT NULL,`content` VARCHAR(254) NOT NULL,`date` VARCHAR(254) NOT NULL);
create table `settings` (`id` INTEGER NOT NULL PRIMARY KEY,`sType` VARCHAR(254) NOT NULL,`value` VARCHAR(254) NOT NULL);

# --- !Downs

drop table `settings`;
drop table `posts_table`;

