import play.PlayScala

name := """glog"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.11.6"

libraryDependencies ++= Seq(
  jdbc,
  cache,
  ws,
  "mysql" % "mysql-connector-java" % "5.1.18",
  "com.typesafe.slick" %% "slick" % "2.1.0",
  "org.slf4j" % "slf4j-nop" % "1.6.4",
  "joda-time" % "joda-time" % "2.5",
  "org.joda" % "joda-convert" % "1.7",
  "com.scalatags" %% "scalatags" % "0.4.1",
  "com.typesafe.play" %% "play-slick" % "0.8.0",
  "com.lihaoyi" %% "upickle" % "0.2.5"
)
