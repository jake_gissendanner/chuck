$(document).ready(function() {
    $('#generate-button').on('click', function(){
        $.get('/api/gen', function(data){
        if (data.success) {
            toastr.success(data.message)
        } else {
            toastr.error(data.message)
        }
        });
    });
    $('.save-settings-button').click(function(e){
        e.preventDefault();
        var form = $('#save-settings-form').serializeArray();

        $.ajax({
            type: "POST",
            url: "/api/saveSettings",
            data: form,
            success: function(data) {
                console.log(data);
            }
        });

        return false;
    });
});